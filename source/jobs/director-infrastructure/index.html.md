---
layout: job_page
title: "Infrastructure Director"
---

The Infrastructure Director reports to the VP of Engineering.

## Responsibilities

* Direct all operations and activities relating to GitLab.com and GitLab Hosted
* Design, plan, and implement the goals to make GitLab.com fast, available, and resilient
* Coordinate infrastructure and application-level security hardening
* Enable all engineers and the public at large to understand current performance issues
* Interview applicants for infrastructure-related positions
* Grow skills in team leads and individual contributors
* Ensures infrastructure issues are well-documented
* Provide necessary infrastructure (e.g. servers, databases, etc.) for company
* Make sure the handbook is used and maintained.
* Identify positions we need to hire for, open up vacancies, and interview applicants.
* Deliver input on promotions, function changes, demotions and firings in consultation with the CEO, CTO, and VP of Engineering
* Push for individual accountability instead of project managers/coordinators.
* Promote GitLab as a great product and place to work by engaging on social media and writing blog posts.

## Requirements

* Experiencing managing multiple teams of engineers
* Proven track record of delivering high-value, high-impact projects
* Strong fundamentals of networks, performance, databases, and scalability
* Familiar with open source work
