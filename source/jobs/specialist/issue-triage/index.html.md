---
layout: job_page
title: "Issue Triage Specialist"
---

## Responsibilities

* Triage issues on [our projects located in the gitlab-org group on GitLab.com](https://gitlab.com/gitlab-org/)
  and in [the GitLab.com Support Tracker](https://gitlab.com/gitlab-com/support-forum/issues/)
  following the [Issue Triage Policies](/handbook/engineering/issues/issue-triage-policies)
* Monitor discussions in those issues, responds in a timely fashion where appropriate (or
  ensures relevant responders are “nudged” to respond)
* Ping team leads or VP of Engineering via chat when outstanding critical
  customer issues are not addressed in a timely fashion
* Identify patterns or common issues reported by community
* Fixes community-reported bugs when possible (approximately 20% of the time)
* Label `Accepting Merge Requests` issues for potential developer candidates in agreement
  with the Platform and/or Discussion leads
* Develop tools to consolidate and reduce duplicate issues
* Create/manage the moderation and terms of use policies
* Communicate important community concerns to the appropriate team
* Documents answers and improves existing documentation
* Write blog posts relevant to the community
* Establish and report metrics on a regular basis, including recommendations and insights
